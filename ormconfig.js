/*
 * I have problem to run typeorm migrations and nest without modifying migrations key, so I created this if statement
 */
const parentFolder =
  __dirname.split('/')[__dirname.split('/').length - 1] !== 'dist'
    ? 'src/'
    : '';
console.log(`Running ormconfig on ${parentFolder}database/migration/*.ts`);

const config = {
  type: 'mysql',
  host: 'mysql-dev',
  port: 3306,
  username: 'root',
  password: 'root',
  database: 'cloud-run-nest-example-db',
  entities: ['dist/**/*.entity.js'],
  migrations: [`${parentFolder}database/migration/*.ts`],
  cli: {
    migrationsDir: 'dist/src/database/migration/',
  },
  logging: true,
};

module.exports = config;
